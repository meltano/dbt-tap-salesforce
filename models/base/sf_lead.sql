with source as (

    select * from {{var('schema')}}.lead

),

renamed as (

    select

        id as lead_id,

        -- keys
        owner_id as owner_id,
        converted_opportunity_id as opportunity_id,
        converted_account_id as account_id,
        converted_contact_id as contact_id,

        -- conversion pipeline
        lead_source as lead_source,
        status,
        is_converted as is_converted,
        converted_date as converted_date,

        -- contact
        first_name as first_name,
        last_name as last_name,
        name as full_name,
        title,
        email,
        phone as work_phone,
        mobile_phone as mobile_phone,

        -- outreach
        is_unread_by_owner as is_unread_by_owner,

        -- company context
        company as company_name,
        city as company_city,
        number_of_employees as number_of_employees,

        -- metadata
        last_activity_date as last_activity_date,
        created_date as created_at,
        last_modified_date as updated_at

    from source

)

select * from renamed
