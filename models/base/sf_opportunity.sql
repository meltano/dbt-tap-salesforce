with source as (

    select * from {{var('schema')}}.opportunity

),

renamed as (

    select

        id as opportunity_id,

        -- keys
        account_id as account_id,
        owner_id as owner_id,

        -- company context
        name as company_name,
        description as opportunity_description,
        type as opportunity_type,

        -- attribution
        lead_source as lead_source,

        -- pipeline
        probability,
        next_step as next_step,
        has_open_activity as has_open_activity,
        stage_name as stage_name,
        forecast_category as forecast_category,
        forecast_category_name as forecast_category_name,
        is_won as is_won,
        is_closed as is_closed,
        close_date as closed_date,
        has_overdue_task as has_over_due_task,

        -- accounting
        amount,

        -- metadata
        last_activity_date as last_activity_date,
        created_date as created_at,
        last_modified_date as updated_at

    from source

)

select * from renamed
