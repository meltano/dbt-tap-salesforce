with source as (

    select * from {{var('schema')}}.contact

),

renamed as (

    select

        id as contact_id,

        -- keys
        owner_id as owner_id,
        account_id as account_id,

        -- personage
        first_name as first_name,
        last_name as last_name,
        name as full_name,
        title,
        email,
        phone as work_phone,
        mobile_phone as mobile_phone,

        -- metadata
        last_activity_date as last_activity_date,
        created_date as created_at,
        last_modified_date as updated_at

    from source

)

select * from renamed
