with source as (

    select * from {{var('schema')}}.opportunityhistory

),

renamed as (

    select

        id as opportunity_history_id,

        -- keys
        opportunity_id as opportunity_id,
        created_by_id as created_by_id,

        -- pipeline
        probability,
        stage_name as stage_name,
        forecast_category as forecast_category,
        amount,
        expected_revenue as expected_revenue,

        -- metadata
        created_date as created_date

    from source

)

select * from renamed
