with source as (

    select * from {{var('schema')}}.user

),

renamed as (

    select

        id as user_id,

        -- keys
        account_id as account_id,

        -- personage
        first_name as first_name,
        last_name as last_name,
        name as full_name,
        username as user_name,
        title,
        email,
        phone as work_phone,
        mobile_phone as mobile_phone,

        -- metadata
        created_date as created_at,
        last_modified_date as updated_at

    from source

)

select * from renamed
