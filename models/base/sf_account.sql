with source as (

    select * from {{var('schema')}}.account

),

renamed as (

    select

        id as account_id,

        -- keys
        parent_id as parent_id,
        owner_id as owner_id,

        -- logistics
        type as account_type,
        billing_street as company_street,
        billing_city as company_city,
        billing_state as company_state,
        billing_country as company_country,
        billing_postal_code as company_zipcode,

        -- details
        name as company_name,
        industry,
        description,
        number_of_employees as number_of_employees,

        -- metadata
        last_activity_date as last_activity_date,
        created_date as created_at,
        last_modified_date as updated_at

    from source

)

select * from renamed
