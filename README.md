# dbt | tap-salesforce

This [dbt](https://github.com/fishtown-analytics/dbt) package contains data models for [tap-salesforce](https://gitlab.com/meltano/tap-salesforce).

It is based on a minimal subset of the models defined in the [package for salesforce by fishtown-analytics](https://github.com/fishtown-analytics/salesforce)

Models included:

  **Base models:** account, contact, lead, user, opportunity, opportunity_history

  **XF:** opportunity_history joined